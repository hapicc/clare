package clare;

import java.net.URISyntaxException;
import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) throws URISyntaxException {
        if (args.length == 0) {
            JFrame cl = new clare.gui.OuterFrame();
            cl.setVisible(true);
        } else {
            new CommandLine(args);
        }

    }

}
