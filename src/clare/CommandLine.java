package clare;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandLine {

    public CommandLine(String[] args) throws URISyntaxException {
        if (args.length == 3) {
            try {
                Clare c = new Clare();
                c.clare(args[0], args[1], args[2]);
            } catch (IOException ex) {
                Logger.getLogger(CommandLine.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        else {
            System.out.println(args.length);
            System.err.println("Incorrect usage of Clare. Proper usage requires one of the following:");
            System.err.println("arguments:");
            System.err.println("<musicXmlFileSourcePath> <dirDestinationPath> <musicName>");
        }
    }

}
