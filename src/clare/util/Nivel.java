package clare.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;

public class Nivel {
    public int find(File file) throws FileNotFoundException {
        try {
            String line = new String(Files.readAllBytes(file.toPath()));

            line = line.replaceFirst("^[@a-zA-Z _\\-\\r\\n0-9]+", "")
                    .replace("{n1,n2,n3,n4,n5,n6,n7,n8,n9,n10}\n\n@data\n", "")
                    .replaceFirst(".+n([0-9]{1,2}$)", "$1");
            return Integer.parseInt(line);
        } catch (Exception e) {
            return 0;
        }
    }
}
