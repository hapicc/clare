package clare.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import weka.classifiers.trees.J48;
import weka.core.Instances;

public class Weka {
   
    public void classify(File treinamento, File teste, File saida) throws FileNotFoundException, IOException, Exception{
        System.out.println("Classificando...");
        BufferedReader breader = null;
        breader = new BufferedReader(new FileReader(treinamento));
        Instances train = new Instances(breader);
        train.setClassIndex(train.numAttributes() - 1);

        breader = new BufferedReader(new FileReader(teste));
        Instances test = new Instances(breader);
        test.setClassIndex(train.numAttributes() - 1);

        breader.close();

        J48 tree = new J48();
        tree.buildClassifier(train);
        Instances labeled = new Instances(test);

        for (int i = 0; i < test.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(test.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(saida));
        String content = labeled.toString();
        writer.write(content);

        writer.close();
        System.out.println("Classificado com Sucesso!");
    }
            
    
}
