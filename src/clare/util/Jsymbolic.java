package clare.util;

import java.io.File;
import java.io.IOException;
import jsymbolic.CommandLine;

public class Jsymbolic {

    public Jsymbolic() {
    }

    public boolean extract(String[] line) throws IOException {
        System.out.println("Extraindo Características...");
        CommandLine c = new CommandLine(line);
        File f = new File(line[1]);
        if (f.exists()) {
            System.out.println("Extraídas com sucesso!");
            return true;
        } else {
            System.out.println("ERRO");
            return false;
        }

    }

}
