package clare;

import clare.util.AceXmlToArff;
import clare.util.Jsymbolic;
import clare.util.Nivel;
import clare.util.Weka;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class Clare {

    private String dir;
    private File ARQ_ARFF_TREINAMENTO;
    private File ARQ_ARFF_TESTE;
    private File ARQ_ARFF_FINAL;
    private File arqFeatures;
    private File arqDesc;
    private File arqXml;
    private File arqMidi;
    private String dirMidi;
    private String dirTemp;
    private String dirArff;
    private String dirMusicXml;
    private ArrayList erro;
    private File musicXmlFile;
    private String musicName;
    private int musicLevel;
    private String musicXmlFilePath;
    private String midiFilePath;


    public ArrayList<String> clare(String musicXmlFileSourcePath, String dirDestinationPath, String musicName) throws URISyntaxException, IOException {
        ArrayList<String> ret = new ArrayList<>();
        erro = new ArrayList<>();
        this.musicName = musicName;
        this.musicXmlFilePath = musicXmlFileSourcePath;
        musicXmlFile = new File(musicXmlFileSourcePath.replace("/", "\\"));

        File dirDestination = new File(dirDestinationPath);
        if (dirDestination.exists()) {
            dir = dirDestination.getAbsolutePath() + "\\clare\\";
        } else {
            dir = "C:\\clare\\";
        }
        dirDestination = new File(dir);
        dirDestination.mkdir();

        dirMidi = dir + "midi\\";
        dirMusicXml = dir + "musicXml\\";
        dirTemp = dir + "temp\\";
        dirArff = dir + "arff\\";

        ARQ_ARFF_TREINAMENTO = new File(dirArff + "ArquivoARFFTreinamento.arff");
        ARQ_ARFF_TESTE = new File(dirTemp + "ArquivoARFFTeste.arff");
        ARQ_ARFF_FINAL = new File(dirTemp + "ArquivoARFFFinal.arff");

        arqFeatures = new File(dirTemp + "features.xml");
        arqDesc = new File(dirTemp + "desc.xml");

        File midiDir = new File(dirMidi);
        File tempDir = new File(dirTemp);
        File xmlDir = new File(dirMusicXml);
        File arffDir = new File(dirArff);
        File nivelFile = new File(dir + "nivel.txt");

        if (!midiDir.exists()) {
            midiDir.mkdir();
        }
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        if (!xmlDir.exists()) {
            xmlDir.mkdir();
        }
        if (!arffDir.exists()) {
            arffDir.mkdir();
        }
        if (nivelFile.exists()) {
            nivelFile.delete();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
        String time = sdf.format(new Date());

        if (!handleXml(time)) {
            System.out.println(erro);
            return erro;
        } else {
            this.musicXmlFilePath = arqXml.getAbsolutePath().replace("\\", "/");
        }

        if (!handleMidi(time)) {
            System.out.println(erro);
            return erro;
        } else {
            this.midiFilePath = arqMidi.getAbsolutePath().replace("\\", "/");
        }

        if (!arqMidi.exists()) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Jsymbolic j = new Jsymbolic();
        String line[] = {arqMidi.getAbsolutePath(), arqFeatures.getAbsolutePath(), arqDesc.getAbsolutePath()};

        try {
            if (!j.extract(line)) {
                erro.add("Erro ao Extrair Características");
                System.out.println(erro);
                return erro;
            }
        } catch (Exception ex) {
            erro.add(ex.toString());
            return erro;
        }

        AceXmlToArff a = new AceXmlToArff();

        if (arqFeatures.exists()) {
            try {
                a.toArff(arqFeatures, ARQ_ARFF_TESTE, ARQ_ARFF_TREINAMENTO);
            } catch (Exception ex) {
                erro.add(ex.toString());
                System.out.println(erro);
                return erro;
            }
        } else {
            erro.add("Erro ao Converter para ARFF: Arquivo Features não existe!");
            System.out.println(erro);
            return erro;
        }

        if (ARQ_ARFF_TESTE.exists()) {
            Weka w = new Weka();
            try {
                w.classify(ARQ_ARFF_TREINAMENTO, ARQ_ARFF_TESTE, ARQ_ARFF_FINAL);
                Nivel n = new Nivel();
                musicLevel = n.find(ARQ_ARFF_FINAL);
            } catch (Exception e) {
                erro.add(e.toString());
                System.out.println(erro);
            }
        } else {
            erro.add("Erro na Classificação: Arquivo ARFF Teste não existe!");
            System.out.println(erro);
        }

        System.out.println("Nivel: " + musicLevel);

        BufferedWriter writer = new BufferedWriter(new FileWriter(nivelFile));
        writer.write(musicName + "\n" + musicLevel);
        writer.close();
        limpaDir(dirTemp);
        ret.add(String.valueOf(musicLevel));
        ret.add(musicXmlFilePath);
        ret.add(midiFilePath);
        return ret;
    }

    public boolean handleXml(String time) {
        try {
            File source = musicXmlFile;
            if (source.exists()) {
                if (source.getAbsoluteFile().toString().contains(".xml")) {
                    arqXml = new File((dirMusicXml + musicName + "_" + time + ".xml").replace(" ", "_"));
                } else if (source.getAbsoluteFile().toString().contains(".mxl")) {
                    arqXml = new File((dirMusicXml + musicName + "_" + time + ".mxl").replace(" ", "_"));
                } else {
                    erro.add("O arquivo deve ser no formato .mxl ou .xml!");
                    return false;
                }
                copy(source, arqXml);
            } else {
                erro.add("O arquivo não existe!");
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            erro.add("Arquivo inválido!");
            return false;
        }

        return true;
    }

    public boolean handleMidi(String time) {
        try {
            arqMidi = new File((dirMidi + musicName + "_" + time + ".mid").replace(" ", "_"));
            String c = "\"C:\\Program Files (x86)\\MuseScore 2\\bin\\MuseScore.exe\" \"" + arqXml
                    + "\" --export-to \"" + arqMidi.getAbsolutePath() + "\"";
            Runtime.getRuntime().exec(c);
        } catch (Exception e) {
            e.printStackTrace();
            erro.add("Arquivo não pode ser convertido para MIDI!");
            return false;
        }
        return true;
    }

    public void copy(File source, File destination) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

    public void limpaDir(String dir) {
        File folder = new File(dir);
        if (folder.isDirectory()) {
            File[] sun = folder.listFiles();
            for (File toDelete : sun) {
                toDelete.delete();
            }
        }
    }

    public String getMusicName() {
        return musicName;
    }

    public int getMusicLevel() {
        return musicLevel;
    }

    public String getMusicXmlFilePath() {
        return musicXmlFilePath;
    }

    public String getMidiFilePath() {
        return midiFilePath;
    }

}
